function [rcx_signal_mupa, rcx_signal_flt]=get_uapassband(dfname, env)
%get_uapassband reads the ocean sonics wav data file with a phsical unit (muPa). 
%Bandpass filtering is applied.
%
%Input:
%       dfname: filename and its path
%       
%       env: a structure that contains parameters for reading and
%       processing of the received waveform. Details are described as
%       below:
%           env.sampling_rate: sampling rate of the received waveform
%           env.f_c: Lowpass filter bandwidth
%           env.BW_LP: Lowpass filter bandwidth
%           env.BW_BP: bandpass filter bandwidth
%           env.flt_order: filter order for the LP and BP filters
%           env.df_stime: start time (in second) of the received waveform
%           env.ob_time: duration (in second) of the received waveform
%
%Output:
%       rcx_signal_mupa: received signal in muPa (not filtered)
%       rcx_signal_flt: received signal in muPa (not filtered)
%
%By Aijun Song.
%LAST update: Sept 11, 2018

%coeff to convert the wavfile reading to muPa @1 m 
vmax=3; %volts
hydrophone_sensitivity=-180.0; %unit dBV re 1 muPa @1m. 
mupa_convert=vmax*10^(-hydrophone_sensitivity/20);

%lowpass filter parameters and coef
flt_Wn=2*env.BW_LP/env.sampling_rate;  
lp_b = fir1(env.flt_order,flt_Wn); lp_a=1;

%bandpass filter parameters and coef
flt_Wn=[2*(env.f_c-0.5*env.BW_BP)/env.sampling_rate, 2*(env.f_c+0.5*env.BW_BP)/env.sampling_rate];
bp_b = fir1(env.flt_order,flt_Wn); bp_a=1;

%locate the received waveform
ssample=round(env.df_stime*env.sampling_rate)+1;       
total_samples=round(env.ob_time*env.sampling_rate);
esample=ssample+total_samples-1;

%Read file%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('[get_uapassband]: Reading the received signal ...');
%read the wav file
[rcx_signal, fs]=audioread(dfname, [ssample esample]);
%disp(['waveform sampling rate: ' num2str(fs)])

%%%%%%%%%%%%%%Convert%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%rcx_signal is converted to mPa @1 m
rcx_signal_mupa=rcx_signal*mupa_convert;

%%%%%%%%%%%%%Bandpass the signal%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rcx_signal_flt=filter(bp_b, bp_a, rcx_signal_mupa);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%End of file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%