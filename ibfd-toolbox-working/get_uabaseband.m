function [rcx_bb_vec]=get_uabaseband(dfname, env)
%get_uabaseband reads the ocean sonics wav data file and convert the signal to 
%the baseband. The processsing steps include: bandpass filtering, carrier-demodulation, lowpass filtering, 
%and matched-filtering.
%
%Input:
%       dfname: filename and its path
%       
%       env: a structure that contains parameters for reading and
%       processing of the received waveform. Details are described as
%       below:
%           env.sampling_rate: sampling rate of the received waveform
%           env.f_c: Lowpass filter bandwidth
%           env.BW_LP: Lowpass filter bandwidth
%           env.BW_BP: bandpass filter bandwidth
%           env.flt_order: filter order for the LP and BP filters
%           env.ds_factor or env.K: either of these two can specify the over-sampling rate
%           env.df_stime: start time (in second) of the received waveform
%           env.ob_time: duration (in second) of the received waveform
%           env.ps_filter: pulse shaping filter (option)
%
%Output:
%       rcx_bb_vec: received signal at the baseband

%By Aijun Song.
%LAST update: Sept 11, 2018

%coeff to convert the wavfile reading to muPa @1 m 
vmax=3; %volts
hydrophone_sensitivity=-180.0; %unit dBV re 1 muPa @1m. 
mupa_convert=vmax*10^(-hydrophone_sensitivity/20);

%lowpass filter parameters and coef
flt_Wn=2*env.BW_LP/env.sampling_rate;  
lp_b = fir1(env.flt_order,flt_Wn); lp_a=1;

%bandpass filter parameters and coef
flt_Wn=[2*(env.f_c-0.5*env.BW_BP)/env.sampling_rate, 2*(env.f_c+0.5*env.BW_BP)/env.sampling_rate];
bp_b = fir1(env.flt_order,flt_Wn); bp_a=1;

%over-sampling factor
if isfield(env, 'ds_factor'),
    tbdown=env.ds_factor;
elseif isfield(env, 'K')
    tbdown=env.sampling_rate/(env.sr*env.K);
else
    disp('[get_uabaseband]: Error. Missing parameters: ds_factor or K.');
    return;
end

%locate the received waveform
ssample=round(env.df_stime*env.sampling_rate)+1;       
total_samples=round(env.ob_time*env.sampling_rate);
esample=ssample+total_samples-1;

%Read file%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('[get_uabaseband]: Reading the received signal and converting to baseband...');
%read the wav file
[rcx_signal, fs]=audioread(dfname, [ssample esample]);
%disp(['waveform sampling rate: ' num2str(fs)])

%%%%%%%%%%%%%%Convert%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%rcx_signal is converted to mPa @1 m
rcx_signal_mupa=rcx_signal*mupa_convert;

%%%%%%%%%%%%%Bandpass the signal%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rcx_signal_flt=filter(bp_b, bp_a, rcx_signal_mupa);

%%%%%%%%%%%%%Carrier Demodulation%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t_vec=([0:length(rcx_signal_flt)-1].')/env.sampling_rate;
data5i=rcx_signal_flt.*cos(2*pi*env.f_c*t_vec);    
data5q=-rcx_signal_flt.*sin(2*pi*env.f_c*t_vec);
data5=data5i+1i*data5q;

%%%%%%%%%%%%%Low-Pass Filtering%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rcx_bb_flt=filter(lp_b, lp_a, data5);

%%%%%%%%%%%%%%Downsampling%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%rcx_bb_down=resample(rcx_bb_flt, tbup, tbdown);    
rcx_bb_down=rcx_bb_flt(1:tbdown:end);

%%%%%%%%%%%%Matched-filtering%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isfield(env, 'ps_filter'),
    rcx_bb_vec=filter(env.ps_filter, 1, rcx_bb_down);     
else
    rcx_bb_vec=rcx_bb_down;
end
disp('[get_uabaseband]: Data converted to baseband.');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%End of file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%