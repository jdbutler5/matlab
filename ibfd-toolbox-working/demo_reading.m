% This program demonstrate the following two functions:
% 1) reading of the recorded waveform, with passband filtering
% 2) conversion to the baseband. 
% 
%
%By Aijun Song
%Last updated: Sep 11, 2018
clear; close all; clc; %clear out the workspace, plots.
prn_fig=0;
output_dir='.\figures\';

sr =5e3;        % symbol rate
beta = 0.125;   % roll-off factor in pulse shaping factor

% Setting the parameters of the receiver
env.sampling_rate= 512e3;   % sampling rate of the received waveform,receiver sampling rate
env.BW_LP= 6.2e3;           % Lowpass filter bandwidth,  1.5/2 times the bandwidth of the singal
env.BW_BP= 6.5e3;           % bandpass filter bandwidth, 1.5 times the bandwidth of the singal
env.flt_order= 2^9 ;        % filter order for the LP and BP filters
env.ds_factor= 8;           % or env.K: either of these two can specify the over-sampling rate
env.df_stime= 0;            % start time (in second) of the received waveform
env.ob_time= 60;            % duration (in second) of the received waveform
env.f_c = 28e3;             % center frequency of the acoustic transmission
env.ps_filter=rcosine(1, env.ds_factor, 'sqrt', beta, 5);%plus shaping filter

%read the acoustic measurements
% fd = cd;   % folder where data is located
fd='C:\Users\mitch\Documents\MATLAB\IBFD-for undergraduates';
fn='RBW1475_20180817_154600.wav';
[~, mfn]=fileparts(fn);
dfname =[fd, '\', fn]; %assemble the file directory and file name
[rcx_signal_mupa, rcx_signal_flt] = get_uapassband(dfname, env);

%replace '_' with '-'
file_id=mfn;
file_id(strfind(mfn,'_'))='-';

%display the signal
figure; 
x_vec=[0: length(rcx_signal_mupa)-1]/env.sampling_rate;
plot(x_vec, rcx_signal_mupa)
grid on;
set(gca, 'Fontsize',12,'FontWeight','bold'); 
xlabel('Time (s)')
ylabel('Amp (\muPa)')
title([file_id ': Raw data']);
%print the output in JPG
if prn_fig,
    print('-djpeg100', [output_dir mfn '_raw'])
end



%display the signal
figure; 
x_vec=[0: length(rcx_signal_flt)-1]/env.sampling_rate;
plot(x_vec, rcx_signal_flt)
grid on;
set(gca, 'Fontsize',12,'FontWeight','bold'); 
xlabel('Time (s)')
ylabel('Amp (\muPa)')
title([file_id ': Filtered']);
%print the output in JPG
if prn_fig,
    print('-djpeg100', [output_dir mfn '_flt'])
end


%convert the signal to baseband
[rcx_bb_vec] = get_uabaseband(dfname, env);
crt_sampling_rate=env.sampling_rate/env.ds_factor;

figure; 
x_vec=[0: length(rcx_bb_vec)-1]/crt_sampling_rate;
plot(x_vec, real(rcx_bb_vec))
grid on;
set(gca, 'Fontsize',12,'FontWeight','bold'); 
xlabel('Time (s)')
ylabel('Amp (\muPa)')
title([file_id ': Real @Baseband']);
if prn_fig,
    print('-djpeg100', [output_dir mfn '_bb'])
end
