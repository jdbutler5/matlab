function varargout = IBFD_GUI_V2(varargin)
% IBFD_GUI_V2 MATLAB code for IBFD_GUI_V2.fig
%      IBFD_GUI_V2, by itself, creates a new IBFD_GUI_V2 or raises the existing
%      singleton*.
%
%      H = IBFD_GUI_V2 returns the handle to a new IBFD_GUI_V2 or the handle to
%      the existing singleton*.
%
%      IBFD_GUI_V2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IBFD_GUI_V2.M with the given input arguments.
%
%      IBFD_GUI_V2('Property','Value',...) creates a new IBFD_GUI_V2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IBFD_GUI_V2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IBFD_GUI_V2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IBFD_GUI_V2

% Last Modified by GUIDE v2.5 06-Dec-2018 16:18:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IBFD_GUI_V2_OpeningFcn, ...
                   'gui_OutputFcn',  @IBFD_GUI_V2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IBFD_GUI_V2 is made visible.
function IBFD_GUI_V2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IBFD_GUI_V2 (see VARARGIN)

% Choose default command line output for IBFD_GUI_V2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes IBFD_GUI_V2 wait for user response (see UIRESUME)
% uiwait(handles.IBFD_GUI);


% --- Outputs from this function are returned to the command line.
function varargout = IBFD_GUI_V2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in addFile.
function addFile_Callback(hObject, eventdata, handles)
% hObject    handle to addFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%opens file select
fileName = uigetfile('', 'Select a File');

%checks if file is loaded/doesn't exist
%I COMMENTED THIS OUT BECAUSE IT DOESN'T WORK
%if uigetfile == 0
%    f = errordlg('Error: please select a file first.', 'File Error')
%end

%I think these variables (audioMatrix, sampleRate, fileName, etc)
%are destroyed as soon as this function ends.
%That means that we need to figure out a way to make them global.

%converts audio to matrix
%If audioread doesn't work, the file is probably not in the path.
disp('Converting audio file to matrix...')
[audioMatrix,sampleRate] = audioread(fileName);
disp('File converted successfully.');

% update gui data
handles.audioMatrix = audioMatrix;
handles.fileName = fileName;
disp(handles.fileName);
guidata(hObject, handles);


%This stuff needs to be moved, but this is the demo for the display.
%It plots the data read in from the audio file into the GUI. 
timeVector = [0 : length(audioMatrix) - 1] / sampleRate;
plot(timeVector, audioMatrix, 'color', [0.620,  0.106,  0.196]);
grid on;
xlabel('Time (s)');
ylabel('Amp (\muPa)');
title('Original Data');

% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes during object creation, after setting all properties.
function conversionSelection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to conversionSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over conversionSelection.
function conversionSelection_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to conversionSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on selection change in conversionSelection.
function conversionSelection_Callback(hObject, eventdata, handles)
% hObject    handle to conversionSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns conversionSelection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from conversionSelection

% Determine the selected data set.
str = get(hObject, 'String');
val = get(hObject,'Value');
% Set current data to the selected data set.
switch str{val};
case 'Baseband' % User selects peaks.
   %handles.current_data = handles.peaks;
   convertToBaseband(handles);
   
   
case 'Passband' % User selects membrane.
   %handles.current_data = handles.membrane;
   convertToPassband(handles);
end
% Save the handles structure.
guidata(hObject,handles)

function convertToPassband(handles)
disp('Converting to passband');
sr =5e3;        % symbol rate
beta = 0.125;   % roll-off factor in pulse shaping factor

% Setting the parameters of the receiver
env.sampling_rate= 512e3;   % sampling rate of the received waveform,receiver sampling rate
env.BW_LP= 6.2e3;           % Lowpass filter bandwidth,  1.5/2 times the bandwidth of the singal
env.BW_BP= 6.5e3;           % bandpass filter bandwidth, 1.5 times the bandwidth of the singal
env.flt_order= 2^9 ;        % filter order for the LP and BP filters
env.ds_factor= 8;           % or env.K: either of these two can specify the over-sampling rate
env.df_stime= 0;            % start time (in second) of the received waveform
env.ob_time= 60;            % duration (in second) of the received waveform
env.f_c = 28e3;             % center frequency of the acoustic transmission
env.ps_filter=rcosine(1, env.ds_factor, 'sqrt', beta, 5);%plus shaping filter

%read the acoustic measurements
% fd = cd;   % folder where data is located
fd='C:\Users\mitch\Documents\MATLAB';
fn='RBW1477_20180817_161600.wav';
[~, mfn]=fileparts(fn);
dfname =[fd, '\', fn]; %assemble the file directory and file name
[rcx_signal_mupa, rcx_signal_flt] = get_uapassband(dfname, env);
%replace '_' with '-'
%file_id=mfn;
%file_id(strfind(mfn,'_'))='-';

%disp(dfname);
%dfname = 'RBW1477_20180817_161600.wav';

timeVector = [0 : length(rcx_signal_flt) - 1] / env.sampling_rate;
%[0.620, 0.106, 0.196]
plot(timeVector, rcx_signal_flt, 'color', [0.620, 0.106, 0.196]);
grid on;
xlabel('Time (s)');
ylabel('Amp (\muPa)');
title('Filtered Data');

%Converts matrix to baseband
function convertToBaseband(handles)
disp('Converting to baseband');
sr =5e3;        % symbol rate
beta = 0.125;   % roll-off factor in pulse shaping factor

% Setting the parameters of the receiver
env.sampling_rate= 512e3;   % sampling rate of the received waveform,receiver sampling rate
env.BW_LP= 6.2e3;           % Lowpass filter bandwidth,  1.5/2 times the bandwidth of the singal
env.BW_BP= 6.5e3;           % bandpass filter bandwidth, 1.5 times the bandwidth of the singal
env.flt_order= 2^9 ;        % filter order for the LP and BP filters
env.ds_factor= 8;           % or env.K: either of these two can specify the over-sampling rate
env.df_stime= 0;            % start time (in second) of the received waveform
env.ob_time= 60;            % duration (in second) of the received waveform
env.f_c = 28e3;             % center frequency of the acoustic transmission
env.ps_filter=rcosine(1, env.ds_factor, 'sqrt', beta, 5);%plus shaping filter

%read the acoustic measurements
% fd = cd;   % folder where data is located
fd='C:\Users\mitch\Documents\MATLAB\IBFD-for undergraduates';
fn='RBW1475_20180817_154600.wav';
[~, mfn]=fileparts(fn);
dfname =[fd, '\', fn]; %assemble the file directory and file name
%[rcx_signal_mupa, rcx_signal_flt] = get_uapassband(dfname, env);

%replace '_' with '-'
file_id=mfn;
file_id(strfind(mfn,'_'))='-';

disp(dfname);
dfname = 'RBW1477_20180817_161600.wav';
[rcx_bb_vec] = get_uabaseband(dfname, env);

timeVector = [0 : length(rcx_bb_vec) - 1] / env.sampling_rate;
%[0.620, 0.106, 0.196]
plot(timeVector, rcx_bb_vec, 'color', [0.620, 0.106, 0.196]);
grid on;
xlabel('Time (s)');
ylabel('Amp (\muPa)');
title('Baseband Data')


